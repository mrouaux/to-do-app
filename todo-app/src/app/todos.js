import React from 'react'
import { render } from 'react-dom'
import TodosApp from './components/TodosApp'
import './css/todos.css'

render(<TodosApp />, document.getElementById('root'))