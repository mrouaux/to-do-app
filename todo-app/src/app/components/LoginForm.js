import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import dataUsers from '../data/dataUsers.js'

const styles = {

    card: {
        minWidth: 275,
        backgroundColor: '#FFFFFF'
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
};

class LoginForm extends Component {

    handleSubmit(e) {

        e.preventDefault()
        // Get inputs values.
        const email = this.refs.email.value.trim()
        const password = this.refs.password.value.trim()

        if (email && password) {
            const inputCredentials = { email, password }
            this.props.submitFormManager(inputCredentials)
        } else {
            alert('You must complete the inputs!')
        }
        this.cleanInputs()
    }

    cleanInputs() {

        this.refs.email.value = ''
        this.refs.password.value = ''
    }

    componentDidMount() {
        this.refs.email.focus()
    }

    render() {

        const { classes } = this.props;
        const bull = <span className={classes.bullet}>•</span>;

        return (
            <Card className={classes.card}>
                <CardContent>
                    <form onSubmit={this.handleSubmit.bind(this)}>
                        <div className="card">
                            <div className="card-content">
                                <div className="form-field">
                                    <label htmlFor="email">Email</label>
                                    <input type="text" placeholder="Email..." id="email" ref="email" />
                                </div>
                                <br />
                                <div className="form-field">
                                    <label htmlFor="password">Password</label>
                                    <input type="password" placeholder="Password..." id="password" ref="password" />
                                </div>
                                <br />
                                <div className="row">
                                    <button className="btn teal lighten-1 white-text col s6 offset-s3 m6 offset-m3">Go!</button>
                                </div>
                                <div className="row">
                                </div>
                            </div>
                        </div>
                    </form>
                </CardContent>
                <CardActions>
                    <Button href="https://www.facebook.com/matias.rouaux" size="small">Conoce al desarrollador</Button>
                </CardActions>
            </Card>
        );
    }
}

LoginForm.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LoginForm);