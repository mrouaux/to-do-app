import React, { Component } from 'react'
import LoginForm from './LoginForm';
import dataUsers from '../data/dataUsers';

const loginStyle = {
    marginTop: 70,
    color: 'rgb(222, 235, 255)'
}
class LoginApp extends Component {

    constructor() {
        super()
        this.state = {
            email: '',
            password: '',
        }

    }

    submitForm(inputCredentials) {

        // Validate user credentials.
        const { email, password } = inputCredentials

        dataUsers.reduce(function (anterior, actual, index, array) {
            if ((anterior.email === email && anterior.password === password)
                || (actual.email === email && actual.password === password)) {
                // Authentication it´s correct.
                // Set data in localStorage and redirect to TodosApp.
                localStorage.setItem("inputCredentials", JSON.stringify(inputCredentials))
                location.href = '/'
            } else {
                // Authentication is incorrect.
                alert('Invalid user or password :(')
            }
        })
    }

    render() {

        return (
            <div className="container">
                <div className="row">
                    <div className="center-align">
                        <h4 style={loginStyle} className="">LOGIN</h4>
                    </div>
                    <div className="col m4 s4 l6 offset-l3 offset-s1 offset-m3">
                        <LoginForm
                            submitFormManager={this.submitForm.bind(this)}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default LoginApp