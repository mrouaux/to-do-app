import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import jsPDF from 'jspdf';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
});

class Table extends Component {

    deleteTask(id) {
        // Sent an id value to TodosApp component.
        this.props.handleDeleteTask(id)
    }

    editTask(id) {
        // Sent id value to TodosApp component.
        this.props.editTask(id)
    }

    printTasks() {
        // Generate a PDF document with the table data.
        var doc = new jsPDF()
        doc.fromHTML($('#testdiv').get(0), 20, 20, {
            'width': 500
        })
        doc.save('Tasks-List.pdf')
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                <List className={classes.root}>
                    <div className="row">
                        <div className="col l6">
                            <Typography variant="display1" color="textSecondary">
                                TODO LIST
                            </Typography>
                        </div>
                        <div className="col l5 offset-l1">
                            <button onClick={this.printTasks} style={{ backgroundColor: '#3F51B5', display: (this.props.todosElements.length == 0) ? 'none' : 'block' }} className="waves-effect waves-light btn col l10 offset-l1 offset-s4">Save as PDF</button>
                        </div>

                        <div className="row col l12">
                            <h6 style={{ display: this.props.todosElements.length === 0 ? 'block' : 'none' }} variant="display" color="textSecondary">
                                Your list is empty :(
                            </h6>
                        </div>
                    </div>
                    <div id="testdiv">
                        <h3 style={{ display: 'none' }}>TODO LIST DETAILS</h3>
                        {
                            this.props.todosElements.map(todo => {
                                return (
                                    <div key={todo.id} className="row" >
                                        <ListItem>
                                            <Avatar>
                                                <i className="small material-icons">check</i>
                                            </Avatar>
                                            <div className="col l10">
                                                <ListItemText primary={todo.title} secondary={todo.description} />
                                            </div>
                                            <button onClick={() => this.deleteTask(todo.id)} style={{ backgroundColor: '#3F51B5', margin: '2px' }} className="waves-effect waves-light btn" ><i className="material-icons">delete</i></button>
                                            <button onClick={() => this.editTask(todo.id)} style={{ backgroundColor: '#3F51B5', margin: '2px' }} className="waves-effect waves-light btn" ><i className="material-icons">edit</i></button>
                                        </ListItem>
                                        <Divider variant="inset" component="li" />
                                    </div>
                                )
                            })
                        }
                    </div>
                </List>
            </div>
        )
    };
}

Table.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Table);
