import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
const uuidv4 = require('uuid/v4')

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

class Todo extends Component {

  constructor() {
    super()
    this.state = {
      idTaskToEdit: '',
    }
  }

  addTask(e) {
    e.preventDefault()
    // Get input values.
    const title = this.refs.title.value
    const description = this.refs.description.value
    const id = uuidv4()
    // idTaskToEdit is set, to know if it is an add or modify event.
    const idTaskToEdit = this.state.idTaskToEdit
    if (title && description) {
      const taskValues = { id, title, description, idTaskToEdit }
      this.cleanInputs()
      this.refs.title.focus()
      this.props.addTaskManager(taskValues)
    } else {
      alert('Please, complete your todo task first!')
    }

  }

  cleanInputs() {
    this.refs.title.value = ''
    this.refs.description.value = ''
  }

  componentDidMount() {
    this.refs.title.focus()
  }

  componentWillReceiveProps() {
    this.cleanInputs()
    this.componentDidMount()
    if (this.props.idFlag !== '') {
      // ID comes with a value, it´s a modify event.
      this.props.taskToEdit.map(task => {
        this.refs.title.value = task.title
        this.refs.description.value = task.description
        this.setState({
          idTaskToEdit: task.id
        })
      })
    }
    else {
      // ID comes empty, it´s an add event.
      this.setState({ idTaskToEdit: '' })
    }

  }

  render() {
    const { classes } = this.props;
    const bull = <span className={classes.bullet}>•</span>;

    return (
      <div>
        <Card className={classes.card}>
          <form onSubmit={this.addTask.bind(this)}>
            <CardContent>
              <Typography className={classes.title} color="textSecondary" gutterBottom>
                TODO
                </Typography>
              <div className="row">
                <div className="input field col s12">
                  <input type="text" placeholder="Task title..." ref="title" />
                </div>
              </div>
              <div className="row">
                <div className="input field col s12">
                  <textarea placeholder="Task description..." className="materialize-textarea" ref="description" ></textarea>
                </div>
              </div>
            </CardContent>
            <div className="row col l12 s12">
              <button type="submit" style={{ backgroundColor: '#3F51B5' }} className="waves-effect waves-light btn col l10 offset-l1 offset-s4">Agregar</button>
            </div>
          </form>
        </Card>
      </div>
    )
  }
}

Todo.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Todo);
