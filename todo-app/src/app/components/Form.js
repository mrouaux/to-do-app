import React, { Component } from 'react'

class Form extends Component {

    constructor() {
        super()
        this.state = {
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(e) {
        // Get inputs values.
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <div className="card">
                <div className="card-action teal lighten-1 white-text">
                    <h3>Login Form</h3>
                </div>
                <div className="card-content">
                    <div className="form-field">
                        <label htmlFor="email">Email</label>
                        <input type="text" placeholder="Email..." id="email" name="email" onChange={this.handleChange} />
                    </div>
                    <br />
                    <div className="form-field">
                        <label htmlFor="password">Password</label>
                        <input type="password" placeholder="Password..." id="password" name="password" onChange={this.handleChange} />
                    </div>
                    <br />
                    <div className="row">
                        <button type="submit" className="btn teal lighten-1 white-text col s6 offset-s3 m6 offset-m3">Go!</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Form