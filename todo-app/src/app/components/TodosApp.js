import React, { Component } from 'react'
import Navbar from './Navbar'
import Todo from './Todo'
import Table from './Table'

class Todos extends Component {

    constructor() {
        super()
        this.state = {
            todosElements: [],
            taskToEdit: [],
            idFlag: ''
        }
    }

    addTask(todosTasks) {

        this.setState({ idFlag: '' }, () => {
            const { title, description, idTaskToEdit } = todosTasks
            if (idTaskToEdit !== '') {
                // ID comes with a value, it´s a modify event.
                this.state.todosElements.map(todoToEdit => {
                    if (todoToEdit.id == idTaskToEdit) {
                        todoToEdit.title = title
                        todoToEdit.description = description
                    }
                })
                // Set data in LocalStorage, in the correct array in order to who has been logged in.
                let inputCredentials = (JSON.parse(localStorage.getItem("inputCredentials")))
                const { email } = inputCredentials
                if (email == "mrouaux10@gmail.com") {
                    localStorage.setItem("todosMatias", JSON.stringify(this.state.todosElements));
                } else {
                    localStorage.setItem("todosCarlos", JSON.stringify(this.state.todosElements));
                }
                this.componentDidMount()
                M.toast({ html: 'Task modified!' })
            }
            else {
                // ID comes empty, it´s an add event.
                console.log('ID comes empty')
                this.setState(() => {
                    this.state.todosElements.push(todosTasks);
                    // Set data in LocalStorage, in the correct array in order to who has been logged in.
                    let inputCredentials = (JSON.parse(localStorage.getItem("inputCredentials")))
                    const { email } = inputCredentials
                    if (email == "mrouaux10@gmail.com") {
                        localStorage.setItem("todosMatias", JSON.stringify(this.state.todosElements));
                    } else {
                        localStorage.setItem("todosCarlos", JSON.stringify(this.state.todosElements));
                    }
                    M.toast({ html: 'Task added!' })
                    return this.state.todosElements
                })
            }
        })

    }

    deleteTask(id) {
        const element = this.state.todosElements;
        for (let i = 0; i < element.length; i++) {
            if (element[i].id === id) {
                this.state.todosElements.splice(i, 1);
            }
        }
        this.setState({
            todosElements: this.state.todosElements
        })
        // Set data in LocalStorage, in the correct array in order to who has been logged in.
        let inputCredentials = (JSON.parse(localStorage.getItem("inputCredentials")))
        const { email } = inputCredentials
        if (email == "mrouaux10@gmail.com") {
            localStorage.setItem("todosMatias", JSON.stringify(this.state.todosElements));
        } else {
            localStorage.setItem("todosCarlos", JSON.stringify(this.state.todosElements));
        }
        M.toast({ html: 'Task deleted!' });
    }

    editTask(id) {

        this.state.todosElements.map(todo => {
            if (todo.id === id) {
                this.state.taskToEdit.push(todo)
                // idFlag is set, to know if it is an add or modify event.
                this.setState({ idFlag: todo.id }, () => {
                    this.setState({
                        taskToEdit: this.state.taskToEdit
                    })
                })
            }
        })


    }

    componentDidMount() {

        // Get tasks elements in order to who has been logged in.
        let inputCredentials = (JSON.parse(localStorage.getItem("inputCredentials")))
        const { email } = inputCredentials
        let { todosMatias, todosCarlos } = []
        if (email == "mrouaux10@gmail.com") {
            if (JSON.parse(localStorage.getItem("todosMatias"))) {
                todosMatias = JSON.parse(localStorage.getItem("todosMatias"))
            } else { todosMatias = [] }
            this.setState({ todosElements: todosMatias })
        } else {
            if (JSON.parse(localStorage.getItem("todosCarlos"))) {
                todosCarlos = JSON.parse(localStorage.getItem("todosCarlos"))
            } else { todosCarlos = [] }
            this.setState({ todosElements: todosCarlos })
        }
    }

    render() {
        // If the user is not logged in, he can not enter to TodosApp.
        if (localStorage.getItem("inputCredentials") == null) {
            location.href = '/login'
        }

        return (
            <div>
                <div className="row">
                    <Navbar />
                </div>
                <div className="row">
                    <div className="col l4 offset-l1">
                        <br />
                        <br />
                        <Todo
                            addTaskManager={this.addTask.bind(this)}
                            taskToEdit={this.state.taskToEdit}
                            idFlag={this.state.idFlag}
                            todosElements={this.state.todosElements}
                        />
                    </div>
                    <div className="col l6 offset-l1">
                        <br />
                        <br />
                        <Table
                            todosElements={this.state.todosElements}
                            handleDeleteTask={this.deleteTask.bind(this)}
                            editTask={this.editTask.bind(this)}
                        />
                    </div>
                </div>
            </div>

        )
    }
}

export default Todos