import React from 'react'
import { render } from 'react-dom'
import LoginApp from './components/LoginApp'
import './css/login.css'

render(<LoginApp />, document.getElementById('root'))