//        ***************************************************
//        *                                                 *
//        *   Author: Matias Rouaux <mrouaux10@gmail.com>   *
//        *                                                 *
//        *   Run the following commands:                   *
//        *            - npm run dev                        *
//        *            - npm run webpack                    *
//        *                                                 *
//        ***************************************************


// NodeJS (Express) File
// Start server
const express = require('express')
const morgan = require('morgan')
const path = require('path')
const app = express()

// Settings
app.set('port', process.env.PORT || 3000)
app.use(morgan('dev'))
app.use(express.json())

// Routes (urls)
app.get('/login', function (req, res) {
    res.sendFile(path.join(__dirname, '/public/login.html'));
});
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '/public/todos.html'));
});

//Static
app.use(express.static(path.join(__dirname, 'public')))

app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`)
})