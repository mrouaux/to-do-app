module.exports = {
    entry: {
        Login: './src/app/login.js',
        Todos: './src/app/todos.js'
    }, 
    output: {
        path: __dirname + '/src/public',
        filename: 'bundle[name].js'
    },
    module:{
        rules:[
            {
                use: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            },
            {
                use: ['style-loader', 'css-loader'],
                test: /\.css$/,
                exclude: /node_modules/
            }
        ]
    }
}